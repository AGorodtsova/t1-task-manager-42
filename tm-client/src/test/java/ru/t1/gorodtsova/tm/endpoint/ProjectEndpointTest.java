package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.gorodtsova.tm.dto.request.project.*;
import ru.t1.gorodtsova.tm.dto.request.user.UserLoginRequest;
import ru.t1.gorodtsova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.gorodtsova.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.gorodtsova.tm.dto.response.project.ProjectListResponse;
import ru.t1.gorodtsova.tm.dto.response.project.ProjectShowByIdResponse;
import ru.t1.gorodtsova.tm.dto.response.user.UserLoginResponse;
import ru.t1.gorodtsova.tm.enumerated.ProjectSort;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;

import static ru.t1.gorodtsova.tm.enumerated.Status.COMPLETED;
import static ru.t1.gorodtsova.tm.enumerated.Status.IN_PROGRESS;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String userToken;

    @Nullable
    private ProjectCreateResponse projectCreateResponse;

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = loginResponse.getToken();
        @Nullable final ProjectCreateRequest projectCreateRequest =
                new ProjectCreateRequest(userToken, "name", "description");
        projectCreateResponse = projectEndpoint.createProject(projectCreateRequest);
    }

    @After
    public void tearDown() {
        projectEndpoint.clearProject(new ProjectClearRequest(userToken));
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void createProject() {
        @Nullable final ProjectCreateRequest request =
                new ProjectCreateRequest(userToken, "new name", "new description");
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        ProjectDTO project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new description", project.getDescription());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(userToken, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest("wrongToken", "new name", "new description")
                )
        );
    }

    @Test
    public void listProject() {
        @Nullable final ProjectListRequest projectListRequest =
                new ProjectListRequest(userToken, ProjectSort.BY_DEFAULT);
        @Nullable final ProjectListResponse projectListResponse = projectEndpoint.listProject(projectListRequest);
        Assert.assertNotNull(projectListResponse.getProjects());
        Assert.assertEquals(projectCreateResponse.getProject().getId(), projectListResponse.getProjects().get(0).getId());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest(userToken, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest("", ProjectSort.BY_DEFAULT))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest("wrongToken", ProjectSort.BY_DEFAULT))
        );
    }

    @Test
    public void showProjectById() {
        @Nullable final ProjectShowByIdRequest projectShowByIdRequest =
                new ProjectShowByIdRequest(userToken, projectCreateResponse.getProject().getId());
        @Nullable final ProjectShowByIdResponse projectShowByIdResponse =
                projectEndpoint.showProjectById(projectShowByIdRequest);
        Assert.assertNotNull(projectShowByIdResponse);
        Assert.assertEquals(projectCreateResponse.getProject().getId(), projectShowByIdResponse.getProject().getId());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.showProjectById(
                        new ProjectShowByIdRequest("wrongToken", projectCreateResponse.getProject().getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(userToken, null))
        );
    }

    @Test
    public void updateProjectById() {
        @NotNull final String projectId = projectCreateResponse.getProject().getId();

        @Nullable final ProjectUpdateByIdRequest projectUpdateByIdRequest =
                new ProjectUpdateByIdRequest(userToken, projectId, "new name", "new description");
        @Nullable final ProjectDTO project =
                projectEndpoint.updateProjectById(projectUpdateByIdRequest).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new description", project.getDescription());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest("wrongToken", projectId, "name", "description")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(userToken, null, "name", "description")
                )
        );
    }

    @Test
    public void clearProject() {
        @Nullable final ProjectListRequest projectListRequest =
                new ProjectListRequest(userToken, ProjectSort.BY_DEFAULT);
        Assert.assertFalse(projectEndpoint.listProject(projectListRequest).getProjects().isEmpty());
        projectEndpoint.clearProject(new ProjectClearRequest(userToken));
        Assert.assertNull(
                projectEndpoint.listProject(new ProjectListRequest(userToken, ProjectSort.BY_DEFAULT)).getProjects()
        );

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.clearProject(new ProjectClearRequest("wrongToken"))
        );
    }

    @Test
    public void removeProjectById() {
        @Nullable final ProjectListRequest projectListRequest =
                new ProjectListRequest(userToken, ProjectSort.BY_DEFAULT);
        Assert.assertFalse(projectEndpoint.listProject(projectListRequest).getProjects().isEmpty());
        @Nullable final ProjectRemoveByIdRequest request =
                new ProjectRemoveByIdRequest(userToken, projectCreateResponse.getProject().getId());
        projectEndpoint.removeProjectById(request);
        Assert.assertNull(
                projectEndpoint.listProject(new ProjectListRequest(userToken, ProjectSort.BY_DEFAULT)).getProjects()
        );

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest("wrongToken", projectCreateResponse.getProject().getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest(userToken, null)
                )
        );
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final String projectId = projectCreateResponse.getProject().getId();
        @Nullable final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(userToken, projectId, IN_PROGRESS);
        @Nullable final ProjectDTO project = projectEndpoint.changeProjectStatusById(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(IN_PROGRESS, project.getStatus());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest("wrongToken", projectId, IN_PROGRESS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest("wrongToken", null, IN_PROGRESS)
                )
        );
    }

    @Test
    public void startProjectById() {
        @Nullable final ProjectDTO project = projectCreateResponse.getProject();
        Assert.assertNotEquals(IN_PROGRESS.toString(), project.getStatus().toString());
        @Nullable final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken, project.getId());
        @Nullable final ProjectDTO newProject = projectEndpoint.startProjectById(request).getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(IN_PROGRESS.toString(), newProject.getStatus().toString());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectById(
                        new ProjectStartByIdRequest("wrongToken", project.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectById(
                        new ProjectStartByIdRequest(userToken, null)
                )
        );
    }

    @Test
    public void completeProjectById() {
        @Nullable final ProjectDTO project = projectCreateResponse.getProject();
        Assert.assertNotEquals(COMPLETED.toString(), project.getStatus().toString());
        @Nullable final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken, project.getId());
        @Nullable final ProjectDTO newProject = projectEndpoint.completeProjectById(request).getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(COMPLETED.toString(), newProject.getStatus().toString());

        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectById(new ProjectCompleteByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectById(
                        new ProjectCompleteByIdRequest("wrongToken", project.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectById(
                        new ProjectCompleteByIdRequest(userToken, null)
                )
        );
    }

}
