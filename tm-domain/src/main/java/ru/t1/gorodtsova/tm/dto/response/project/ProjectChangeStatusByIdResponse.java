package ru.t1.gorodtsova.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
