package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.ProjectSort;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO model);

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    void set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    void removeAll();

    void removeAll(@Nullable String userId);

    @Nullable
    ProjectDTO removeOne(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable
    ProjectDTO removeOneById(@Nullable String id);

    @NotNull
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

}
