package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO add(@Nullable UserDTO user);

    @NotNull
    Collection<UserDTO> add(@Nullable Collection<UserDTO> users);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Collection<UserDTO> set(@Nullable Collection<UserDTO> users);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    void removeAll();

    @NotNull
    UserDTO removeOne(@Nullable UserDTO user);

    @NotNull
    UserDTO removeOneById(@Nullable String id);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    UserDTO setPassword(String id, String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
