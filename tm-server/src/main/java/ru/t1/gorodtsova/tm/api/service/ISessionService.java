package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO create(@NotNull SessionDTO session);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @NotNull
    SessionDTO removeOneById(@Nullable String id);

    boolean existsById(@Nullable String id);

}
